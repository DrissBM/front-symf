# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Installing

First run the command below to install all the dependecies

```
yarn install
```

## Developing

Before starting to develop you need to add a file `.env.local` into the configs folder.

and fill it like this :

```
REACT_APP_API_BASE='/api'
```

While developing you will run:

```
yarn start
```

Now you can access to your front following this link "http://localhost:3000"

Well done ! Now your front is running but it need the back to run correctly.

## Install the back

Clone this repo https://gitlab.com/DrissBM/back-symf and follow the `README.md` of the project.

Once it's done and your back is running, in your package.json check if the "proxy" correspond to the url used by your back, for exemple : http://localhost:8000.

## Access to storybook

To access to storybook, run

```
yarn storybook
```

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
