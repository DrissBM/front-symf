import { withKnobs } from "@storybook/addon-knobs";
import "!style-loader!css-loader!sass-loader!./scss-loader.scss";
import React from "react";
import { addDecorator } from "@storybook/react";
import { MemoryRouter } from "react-router";

addDecorator((story) => <MemoryRouter initialEntries={["/"]}>{story()}</MemoryRouter>);

export const decorators = [withKnobs];

const customViewports = {
  iPhone678: {
    name: "iPhone 6/7/8",
    styles: {
      width: "375px",
      height: "667px",
    },
  },
  iPhone678Plus: {
    name: "iPhone 6/7/8 Plus",
    styles: {
      width: "414px",
      height: "736px",
    },
  },
  iPhoneX: {
    name: "iPhone X",
    styles: {
      width: "375px",
      height: "812px",
    },
  },
  iPad: {
    name: "iPad",
    styles: {
      width: "768px",
      height: "1024px",
    },
  },
};

export const parameters = {
  options: {
    storySort: {
      order: ["Atoms", "Molecules", "Organisms"],
    },
  },
  viewport: {
    viewports: customViewports,
  },
};
