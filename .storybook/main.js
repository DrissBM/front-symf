module.exports = {
  addons: [
    "@storybook/addon-storysource",
    "@storybook/addon-docs",
    "@storybook/preset-create-react-app",
    "@storybook/addon-knobs",
    "@storybook/addon-viewport",
  ],
  stories: ["../src/**/*.stories.@(tsx|mdx)"],
  webpackFinal: async (config) => {
    if (process.env.NODE_ENV === "production") {
      config.output.publicPath = "/storybook/";
    }
    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      loader: require.resolve("babel-loader"),
      options: {
        presets: [["react-app", { flow: false, typescript: true }]],
      },
    });
    config.resolve.extensions.push(".ts", ".tsx", ".mdx");
    return config;
  },
  managerWebpack: async (config) => {
    if (process.env.NODE_ENV === "production") {
      config.output.publicPath = "/storybook/";
    }
    return config;
  },
};
