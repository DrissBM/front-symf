import { create } from "@storybook/theming/create";
import { addons } from "@storybook/addons";

import LOGO from "./assets/logo.png";

addons.setConfig({
  theme: create({
    base: "light",
    colorPrimary: "#0078cf",
    colorSecondary: "#0078cf",
    // UI
    appBg: "#eeeceb",
    appContentBg: "#181a1b",
    appBorderRadius: 0,
    // Typography
    fontBase: "sans-serif",
    // Text colors
    textColor: "#8d00cf",
    textInverseColor: "rgba(255,255,255,0.9)",
    // Toolbar default and active colors
    barTextColor: "#8d00cf",
    barSelectedColor: "#0078cf",
    // Form colors
    inputBg: "#ffffff",
    inputTextColor: "#8d00cf",
    inputBorderRadius: 4,
    brandTitle: "Spread",
    brandImage: LOGO,
  }),
});
