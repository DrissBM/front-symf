import React, { FunctionComponent } from "react"; // importing FunctionComponent
import { Header } from "../header/Header";

type PageProps = {
  /** Layout of the page */
  pageClass?: string;
};

export const Page: FunctionComponent<PageProps> = ({ children, pageClass }) => {
  return (
    <div className="layout__page">
      <Header />
      <main className={pageClass ? pageClass : ""}>{children}</main>
    </div>
  );
};
