import React, { useContext, useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { Button } from "src/components/atoms/button/button";
import { Icon } from "src/components/atoms/icon/icon";
import { Text } from "src/components/atoms/text/text";
import { AppContext } from "src/contexts/appContext";

export const Header = () => {
  const history = useHistory();
  const { apiToken, setApiToken, userName } = useContext(AppContext);
  const [isNavOpen, setIsNavOpen] = useState<boolean>(false);
  const logout = () => {
    localStorage.removeItem("apiToken");
    setApiToken("");
    history.push("/");
  };
  const openNav = () => {
    setIsNavOpen(!isNavOpen);
  };
  useEffect(() => {
    if (isNavOpen) {
      document.body.classList.add("modalOpen");
    } else {
      document.body.classList.remove("modalOpen");
    }
  }, [isNavOpen]);
  return (
    <header>
      <Link to="/">
        <div className="logo">
          <Icon name="logo" />
          <Text type="p2" color="blue">
            SPREAD
          </Text>
        </div>
      </Link>
      <nav className={`mainNav ${isNavOpen ? "open" : ""}`}>
        <ul>
          <li>
            <Link to="/projects">Projects</Link>
          </li>
          <li>
            <Link to="/import">Add Project</Link>
          </li>
        </ul>
        {apiToken ? (
          <Button color="primary" onClick={logout}>
            Logout
          </Button>
        ) : (
          <Button color="primary" link="/auth">
            Login
          </Button>
        )}
      </nav>

      <p className="userName">{userName}</p>

      {apiToken ? (
        <Button color="white" outline small onClick={logout}>
          Logout
        </Button>
      ) : (
        <Button color="white" outline small link="/auth">
          Login
        </Button>
      )}
      <button className="mobile-menu" onClick={openNav}>
        <div className={`burger-menu ${isNavOpen ? "open" : ""}`} />
      </button>
    </header>
  );
};
