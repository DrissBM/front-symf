export const localForageKeys = {
  accessToken: "accessToken",
  readItems: "readItems",
  version: "version",
  backHome: "backHome",
};
