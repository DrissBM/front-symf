export const bemModifier = (modifiers: object, base: string) => {
  let classNames: Array<string> = [base];
  Object.keys(modifiers).forEach((key) => {
    // @ts-ignore
    if (typeof modifiers[key] === "string" && modifiers[key]) {
      // @ts-ignore
      classNames.push(base + "--" + modifiers[key]);
      // @ts-ignore
    } else if (modifiers[key] === true) {
      classNames.push(base + "--" + key);
    }
  });

  return classNames.join(" ");
};
