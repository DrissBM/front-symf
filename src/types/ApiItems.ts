export type environnementVar = {
  name: string;
  value: string;
};

export type ProjectCreate = {
  name: string;
  techno: string;
  rootDir: string;
  buildCmd: string;
  outputDir: string;
  installCmd: string;
  envVar: Array<environnementVar>;
  gitlabId: number;
  gitlabName: string;
  locked: boolean;
  branch: string;
  commit: string;
  gitlabLastModif: string;
  gitlabPath: string;
};

export type ProjectType = {
  id: number;
  name: string;
  techno: "react" | "symfony";
  rootDir: string;
  buildCmd: string;
  outputDir: string;
  installCmd: string;
  envVar: Array<environnementVar>;
  URL: null | string;
  gitlabId: number;
  gitlabName: string;
  locked: boolean;
  branch: string;
  commit: string;
  gitlabLastModif: string;
  gitlabPath: string;
  state: State;
  users: Array<string>;
};

type State = "onHold" | "success" | "failed";

export type GitlabProjectType = {
  id: number;
  path: string;
  default_branch: string;
  last_activity_at: string;
  path_with_namespace: string;
  visibility: "private" | "public";
  branch: Array<any>;
};
