import React from "react";
import "./assets/scss/App.scss";
import { AppRouter } from "./AppRouter";
import { AppContextProvider } from "./contexts/appContext";

export function App() {
  return (
    <AppContextProvider>
      <div className="App">
        <AppRouter />
      </div>
    </AppContextProvider>
  );
}
