import * as React from "react";
import { SpreadApi } from "src/services/SpreadApi";

export type AppContextType = AppContextConstant & AppContextState;

export type AppContextConstant = {
  api: SpreadApi;
};

export type AppContextState = {
  apiToken: string;
  setApiToken: (apiToken: string) => void;
  userName: string;
  setUserName: (apiToken: string) => void;
};

export function AppContextInitial(): AppContextType {
  return {
    api: new SpreadApi(),
    apiToken: "",
    setApiToken: () => console.warn("No Context"),
    userName: "",
    setUserName: () => console.warn("No Context"),
  };
}

const InitialContext = AppContextInitial();

export const AppContext = React.createContext<AppContextType>(InitialContext);

export const AppContextConsumer = AppContext.Consumer;

export const AppContextProvider = ({ children }) => {
  const [apiToken, setApiToken] = React.useState<string>(InitialContext.apiToken);
  const [userName, setUserName] = React.useState<string>(InitialContext.userName);
  const contextStates: AppContextState = {
    apiToken,
    setApiToken,
    userName,
    setUserName,
  };

  return (
    <AppContext.Provider value={{ ...InitialContext, ...contextStates }}>
      {children}
    </AppContext.Provider>
  );
};
