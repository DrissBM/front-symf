import React from "react";
import { Text } from "./text";
import { withKnobs, text } from "@storybook/addon-knobs";

export const DefaultText = () => <Text>{text("Name", "James")}</Text>;

export const P1 = () => <Text type="p1">P1 Text</Text>;
export const P2 = () => <Text type="p2">P2 Text</Text>;
export const P3 = () => <Text type="p3">P3 Text</Text>;
export const P4 = () => <Text type="p4">P4 Text</Text>;
export const LinkText = () => <Text type="link">Link Text</Text>;
export const Label = () => <Text type="label">Link Text</Text>;
export const OrangeText = () => <Text color="blue">Colored Text</Text>;
export const PaleBrownText = () => <Text color="pink">Colored Text</Text>;
export const WhiteText = () => (
  <div style={{ background: "#888" }}>
    <Text color="white">Colored Text</Text>
  </div>
);
export const BlackText = () => <Text color="black">Colored Text</Text>;
export const CenterText = () => <Text center>Center Text</Text>;

const component = {
  title: "Atoms/Text",
  component: Text,
  decorators: [withKnobs],
};
export default component;
