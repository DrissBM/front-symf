import React, { FunctionComponent } from "react"; // importing FunctionComponent
import { bemModifier } from "../../../helpers/bemModifier";

type TextProps = {
  /** Color of button */
  type?: "p1" | "p2" | "p3" | "p4" | "link" | "label" | "intro";
  color?: "blue" | "pink" | "white" | "black";
  className?: string;
  center?: boolean;
};

export const Text: FunctionComponent<TextProps> = ({ children, className, ...modifiers }) => {
  if (children) {
    return <p className={bemModifier(modifiers, "text") + " " + className}>{children}</p>;
  } else {
    return null;
  }
};
