import React, { FunctionComponent } from "react"; // importing FunctionComponent
import { bemModifier } from "../../../helpers/bemModifier";
import { Icon } from "../icon/icon";
import { Text } from "../text/text";

type DropdownProps = {
  /** Color of button */
  name: string;
  value: string;
  setValue: any;
  icon?: boolean;
  options: {};
  error?: any;
};

export const Dropdown: FunctionComponent<DropdownProps> = ({
  children,
  name,
  icon,
  options,
  value,
  setValue,
  error,
  ...modifiers
}) => {
  const listOptions = Object.keys(options).map((key, index) => {
    const label = options[key];
    return (
      <option key={index} value={label}>
        {label}
      </option>
    );
  });
  return (
    <>
      <div className={bemModifier(modifiers, "dropdown")}>
        <label htmlFor={name}>{name}</label>
        <div className="list">
          <Icon name="chevronDown" />
          {icon && value === ("react" || "symfony") && (
            <div className="icon-select">
              <Icon name={value} />
            </div>
          )}

          <select
            value={value}
            onChange={(e) => setValue(e.target.value)}
            id={name}
            name={name}
            required
          >
            <option value="" disabled hidden></option>
            {listOptions}
          </select>
        </div>
      </div>
      {error && (
        <Text type="p3" color="blue">
          {error}
        </Text>
      )}
    </>
  );
};
