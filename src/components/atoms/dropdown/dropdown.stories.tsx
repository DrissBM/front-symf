import React, { useEffect, useState } from "react";
import { Dropdown } from "./dropdown";

const options: Array<string> = ["react", "symfony"];
export const DefaultDropdown = () => {
  const [optionSelected, setOptionSelected] = useState<"react" | "symfony" | "gitlab">("react");
  return (
    <Dropdown
      name="Category"
      options={options}
      value={optionSelected}
      setValue={setOptionSelected}
      icon
    />
  );
};
export const ErrorDropdown = () => {
  const [optionSelected, setOptionSelected] = useState<"react" | "symfony" | "gitlab">("react");
  return (
    <>
      <Dropdown
        name="Category"
        options={options}
        value={optionSelected}
        setValue={setOptionSelected}
        error={"Please select a category"}
      />
    </>
  );
};

const component = {
  title: "Atoms/Dropdown",
  component: Dropdown,
};
export default component;
