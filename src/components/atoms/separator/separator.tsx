import React, { FunctionComponent } from "react"; // importing FunctionComponent
import { bemModifier } from "../../../helpers/bemModifier";

export const Separator: FunctionComponent = ({ children, ...modifiers }) => {
  return (
    <div className={bemModifier(modifiers, "separator")}>{children && <span>{children}</span>}</div>
  );
};
