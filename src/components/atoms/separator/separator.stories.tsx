import React from "react";
import { Separator } from "./separator";

export const DefaultSeparator = () => <Separator />;

export const TextSeparator = () => <Separator>Text Separator</Separator>;

const component = {
  title: "Atoms/Separator",
  component: Separator,
};
export default component;
