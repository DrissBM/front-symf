import React, { FunctionComponent, useState } from "react"; // importing FunctionComponent
import { bemModifier } from "../../../helpers/bemModifier";
import { Button } from "../button/button";
import { Text } from "../text/text";

type InputProps = {
  /** Color of button */
  name: string;
  type: string;
  noTitle?: boolean;
  value: string | number | undefined;
  setValue: any;
  step?: any;
  label?: string;
  disabled?: boolean;
  error?: any;
  editable?: boolean;
  placeholder?: string;
};

export const Input: FunctionComponent<InputProps> = ({
  children,
  name,
  type,
  value,
  setValue,
  step,
  label,
  disabled,
  error,
  editable,
  placeholder,
  ...modifiers
}) => {
  const [isDisabled, setIsDisabled] = useState<boolean | undefined>(disabled);
  return (
    <>
      <div className={bemModifier(modifiers, "input")}>
        <label htmlFor={name}>{label ? label : name}</label>
        <div className="editable">
          <input
            value={value}
            onChange={(e) => setValue(e.target.value)}
            type={type}
            id={name}
            placeholder={placeholder}
            name={name}
            step={step}
            style={{ borderColor: error ? "#f95111" : "" }}
            disabled={isDisabled}
            required
          />
          {editable && (
            <Button
              color="white"
              outline
              onClick={() => {
                setIsDisabled(!isDisabled);
              }}
            >
              Edit
            </Button>
          )}
        </div>
      </div>
      {error && (
        <Text type="p3" color="blue">
          {error}
        </Text>
      )}
    </>
  );
};
