import React, { useState } from "react";
import { Input } from "./input";

export const DefaultInput = () => {
  const [value, setValue] = useState<string>("pirates-of-the-malibustation");

  return (
    <div style={{ background: "#181a1b", padding: "8px" }}>
      <Input name="Project name" type="text" value={value} setValue={setValue} />
    </div>
  );
};
export const InputDisabled = () => {
  const [value, setValue] = useState<string>("pirates-of-the-malibustation");
  return <Input name="Project name" type="text" value={value} setValue={setValue} disabled />;
};

export const InputError = () => {
  const [value, setValue] = useState<string>("pirates-of-the-malibustation");
  return (
    <Input
      name="Project name"
      type="text"
      value={value}
      setValue={setValue}
      error={"Please fill the input"}
    />
  );
};

const component = {
  title: "Atoms/Input",
  component: Input,
};
export default component;
