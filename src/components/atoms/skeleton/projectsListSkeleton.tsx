import React, { FunctionComponent } from "react";
import { MySkeleton } from "./mySkeleton";
// import { MySkeleton, SkeletonType } from "./listItemSkeleton";
export type MyProjectsListSkeletonProps = {
  numberItem: number;
  // type?: SkeletonType;
};
export const ProjectsListSkeleton: FunctionComponent<MyProjectsListSkeletonProps> = ({
  numberItem,
}) => {
  return (
    <ul className="projectsList">
      {[...Array(numberItem)].map((_, index) => {
        return (
          <li className="project" key={index}>
            <div className="content">
              <MySkeleton className="name" type="text" width={80} height={22} />
              <MySkeleton className="icon" type="circle" width={16} height={16} />
              <MySkeleton className="lastModif" type="text" width={180} height={22} />
            </div>
            <MySkeleton type="rect" width={84} height={35} />
          </li>
        );
      })}
    </ul>
  );
};
