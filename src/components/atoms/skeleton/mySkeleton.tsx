import React, { FunctionComponent } from "react";
import { Skeleton } from "@material-ui/lab";

export type SkeletonProps = {
  type?: SkeletonType;
  width?: number;
  height?: number;
  className?: string;
};
export type SkeletonType = "text" | "circle" | "rect";

export const MySkeleton: FunctionComponent<SkeletonProps> = ({
  type,
  width,
  height,
  className,
}) => {
  return <Skeleton className={className} variant={type} width={width} height={height} />;
};
