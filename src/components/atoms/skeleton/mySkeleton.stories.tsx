import React from "react";
import { MySkeleton } from "./mySkeleton";

export const Default = () => <MySkeleton type="circle" width={50} height={50} />;
export const Rect = () => <MySkeleton type="rect" width={50} height={50} />;
export const Text = () => <MySkeleton type="text" width={40} height={11} />;
export const LongText = () => <MySkeleton type="text" width={250} height={11} />;

const component = {
  title: "Atoms/Skeleton/MySkeleton",
  component: MySkeleton,
};
export default component;
