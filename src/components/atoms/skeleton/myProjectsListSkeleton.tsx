import React, { FunctionComponent } from "react";
import { Button } from "../button/button";
import { Icon } from "../icon/icon";
import { MySkeleton } from "./mySkeleton";
// import { MySkeleton, SkeletonType } from "./listItemSkeleton";
export type MyProjectsListSkeletonProps = {
  numberItem: number;
  // type?: SkeletonType;
};
export const MyProjectsListSkeleton: FunctionComponent<MyProjectsListSkeletonProps> = ({
  numberItem,
}) => {
  return (
    <ul className="myProjectsList">
      {[...Array(numberItem)].map((_, index) => {
        return (
          <li className="myProject" key={index}>
            <div className="separate">
              <MySkeleton type="circle" width={16} height={16} />
              <MySkeleton className="text-overflow" type="text" width={10} height={22} />
              <MySkeleton type="rect" width={95} height={35} />
            </div>
            <div className="content">
              <Icon name="branch" color="white" />
              <MySkeleton type="text" width={150} height={22} />
            </div>
            <div className="content">
              <Icon name="commit" color="white" />
              <MySkeleton type="text" width={150} height={22} />
            </div>
            <div className="separate">
              <Icon name="gitlab" />
              <MySkeleton className="text-overflow" type="text" width={10} height={22} />
              <Button icon="settings" notClickable />
            </div>
          </li>
        );
      })}
    </ul>
  );
};
