import React, { FunctionComponent } from "react"; // importing FunctionComponent
import { bemModifier } from "../../../helpers/bemModifier";

type BoxProps = {
  className?: string;
};

export const Box: FunctionComponent<BoxProps> = ({ children, className, ...modifiers }) => {
  if (children) {
    return <div className={bemModifier(modifiers, "box") + " " + className}>{children}</div>;
  } else {
    return null;
  }
};
