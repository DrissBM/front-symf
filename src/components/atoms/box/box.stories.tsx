import React from "react";
import { Box } from "./box";

export const DefaultBox = () => <Box>Default Box</Box>;

const component = {
  title: "Atoms/Box",
  component: Box,
};
export default component;
