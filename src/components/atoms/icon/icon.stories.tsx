import React from "react";
import { Icon, AllIconNames } from "./icon";

export const DefaultIcon = () => <Icon name="accident"></Icon>;

export const AllIcons = () => {
  return (
    <ul style={{ display: "flex", flexWrap: "wrap" }}>
      {AllIconNames.map((iconName, index) => (
        <li style={{ display: "flex", alignItems: "center", padding: "10px" }} key={index}>
          <Icon name={iconName} />{" "}
          <span style={{ paddingLeft: "10px", color: "white" }}>{iconName}</span>
        </li>
      ))}
    </ul>
  );
};

const component = {
  title: "Atoms/Icon",
  component: Icon,
};
export default component;
