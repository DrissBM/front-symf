import React, { FunctionComponent } from "react"; // importing FunctionComponent
import { bemModifier } from "../../../helpers/bemModifier";
import { ReactComponent as Accident } from "src/assets/icons/accident.svg";
import { ReactComponent as Gitlab } from "src/assets/icons/gitlab.svg";
import { ReactComponent as Branch } from "src/assets/icons/branch.svg";
import { ReactComponent as Commit } from "src/assets/icons/commit.svg";
import { ReactComponent as ReactIcon } from "src/assets/icons/react.svg";
import { ReactComponent as Symfony } from "src/assets/icons/symfony.svg";
import { ReactComponent as OpenLink } from "src/assets/icons/openLink.svg";
import { ReactComponent as Warning } from "src/assets/icons/warning.svg";
import { ReactComponent as Settings } from "src/assets/icons/settings.svg";
import { ReactComponent as Logo } from "src/assets/icons/logo.svg";
import { ReactComponent as Search } from "src/assets/icons/search.svg";
import { ReactComponent as Add } from "src/assets/icons/add.svg";
import { ReactComponent as ChevronDown } from "src/assets/icons/chevron_down.svg";
import { ReactComponent as Lock } from "src/assets/icons/lock.svg";
import { ReactComponent as LockOpen } from "src/assets/icons/lock-open.svg";
import { ReactComponent as Cross } from "src/assets/icons/cross.svg";
import { ReactComponent as Tools } from "src/assets/icons/tools.svg";

export const AllIconNames = [
  "accident",
  "gitlab",
  "branch",
  "commit",
  "react",
  "symfony",
  "openLink",
  "warning",
  "settings",
  "logo",
  "search",
  "add",
  "chevronDown",
  "lock",
  "lockOpen",
  "cross",
  "tools",
] as const; // TS 3.4

type IconsTuple = typeof AllIconNames; // readonly
export type IconNames = IconsTuple[number]; // union type

type IconProps = {
  /** Color of button */
  color?: "pink" | "white" | "black" | "grey";
  name: IconNames;
};

const icons: { [key in IconNames]: JSX.Element } = {
  accident: <Accident />,
  gitlab: <Gitlab />,
  branch: <Branch />,
  commit: <Commit />,
  react: <ReactIcon />,
  symfony: <Symfony />,
  openLink: <OpenLink />,
  warning: <Warning />,
  settings: <Settings />,
  logo: <Logo />,
  search: <Search />,
  add: <Add />,
  chevronDown: <ChevronDown />,
  lock: <Lock />,
  lockOpen: <LockOpen />,
  cross: <Cross />,
  tools: <Tools />,
};

export const Icon: FunctionComponent<IconProps> = ({ children, name, ...modifiers }) => {
  return <div className={bemModifier(modifiers, "icon")}>{icons[name]}</div>;
};
