import React from "react";
import { Button } from "./button";

export const DefaultButton = () => <Button>Default Button</Button>;

export const PrimaryButton = () => (
  <Button color="primary" icon="openLink" small>
    Open Button
  </Button>
);

export const IconLeft = () => (
  <Button color="secondary" iconLeft="gitlab" large tall>
    Open Button
  </Button>
);

export const SecondaryButton = () => <Button color="secondary">Import</Button>;

export const LargeButton = () => (
  <Button color="primary" large>
    Hello Button
  </Button>
);

export const SecondaryOutlineButton = () => (
  <Button color="white" outline>
    Edit
  </Button>
);

export const Add = () => (
  <Button color="white" icon="add" outline tall>
    Add project
  </Button>
);

export const Settings = () => <Button icon="settings" />;

const component = {
  title: "Atoms/Button",
  component: Button,
};
export default component;
