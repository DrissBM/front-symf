import React, { FunctionComponent } from "react"; // importing FunctionComponent
import { Link } from "react-router-dom";
import { bemModifier } from "../../../helpers/bemModifier";
import { Icon, IconNames } from "../icon/icon";

type ButtonProps = {
  /** Color of button */
  icon?: IconNames;
  iconLeft?: IconNames;
  tall?: boolean;
  color?: "primary" | "secondary" | "white" | "black" | "yellow" | "red"; // Change the required prop to an optional prop.
  outline?: boolean;
  large?: boolean;
  small?: boolean;
  ahref?: string;
  link?: string | null;
  target?: "_blank";
  className?: string;
  notClickable?: boolean;
  onClick?();
};

export const Button: FunctionComponent<ButtonProps> = ({
  children,
  ahref,
  target,
  link,
  icon,
  iconLeft,
  className,
  onClick,
  ...modifiers
}) => {
  return (
    <button
      className={bemModifier(modifiers, "button") + ` ${className} ${iconLeft ? "iconLeft" : ""}`}
      onClick={onClick ? onClick : undefined}
    >
      {iconLeft && <Icon name={iconLeft} color="white"></Icon>}
      {ahref && (
        <a href={ahref} target={target}>
          {ahref}
        </a>
      )}
      {link && <Link to={link} />}
      {children}
      {icon && <Icon name={icon} color="white"></Icon>}
    </button>
  );
};
