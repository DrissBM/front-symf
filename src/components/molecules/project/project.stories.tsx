import React from "react";
import { Project } from "./project";

export const DefaultProject = () => (
  <Project name="Pirates_of_the_Malibustation" lock={true} lastModif="20h ago" id={1} />
);

const component = {
  title: "Molecules/Project",
  component: Project,
};
export default component;
