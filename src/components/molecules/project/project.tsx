import React, { FunctionComponent } from "react"; // importing FunctionComponent
import { Button } from "src/components/atoms/button/button";
import { Icon } from "src/components/atoms/icon/icon";
import { bemModifier } from "../../../helpers/bemModifier";
import moment from "moment";

type ProjectProps = {
  /** Color of button */
  name: string;
  lock?: boolean;
  lastModif: string;
  id: number;
};

export const Project: FunctionComponent<ProjectProps> = ({
  lastModif,
  lock,
  name,
  id,
  children,
  ...modifiers
}) => {
  return (
    <li className={bemModifier(modifiers, "project")}>
      <div className="content">
        <span className="name">{name}</span>
        <Icon name={lock ? "lock" : "lockOpen"} color="grey" />
        <span className="lastModif">{moment(lastModif).fromNow()}</span>
      </div>
      <Button color="secondary" link={`config/${id}`}>
        Import
      </Button>
    </li>
  );
};
Project.defaultProps = {
  // This value is adopted when name prop is omitted.
};
