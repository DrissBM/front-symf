import React from "react";
import { MyProject } from "./myProject";

export const DefaultMyProject = () => (
  <MyProject
    id={1}
    gitlabId={123123}
    name="Pirates_of_the_Malibustation"
    techno="react"
    branch="master"
    commit="last commit"
    URL="http://google.com"
    state="onHold"
    lastModif="20h ago"
  />
);

const component = {
  title: "Molecules/MyProject",
  component: MyProject,
};
export default component;
