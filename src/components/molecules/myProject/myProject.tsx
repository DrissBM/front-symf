import React, { FunctionComponent } from "react"; // importing FunctionComponent
import moment from "moment";
import { Button } from "src/components/atoms/button/button";
import { Icon, IconNames } from "src/components/atoms/icon/icon";
import { bemModifier } from "../../../helpers/bemModifier";
// import { getRoute, routes } from "src/AppRouter";

type MyProjectProps = {
  /** Color of button */
  id: number;
  gitlabId: number;
  name: string;
  techno: IconNames;
  branch: string;
  commit: string;
  lastModif: string;
  URL: string | null;
  state: string;
};

export const MyProject: FunctionComponent<MyProjectProps> = ({
  id,
  gitlabId,
  name,
  techno,
  branch,
  commit,
  lastModif,
  URL,
  state,
  ...modifiers
}) => {
  return (
    <li className={bemModifier(modifiers, "myProject")}>
      <div className="separate">
        <Icon name={techno} color="white" />
        <span className="text-overflow">{name}</span>
        {state === "success" && URL ? (
          <Button color="primary" icon="openLink" small ahref={URL} target="_blank">
            Open
          </Button>
        ) : state === "onHold" ? (
          <Button color="yellow" icon="tools" small notClickable>
            onHold
          </Button>
        ) : (
          <Button color="red" icon="warning" small link={`/config/${gitlabId}/${id}`}>
            Failed
          </Button>
        )}
      </div>
      <div className="content">
        <Icon name="branch" color="white" />
        {branch}
      </div>
      <div className="content">
        <Icon name="commit" color="white" />
        {commit}
      </div>
      <div className="separate">
        <Icon name="gitlab" />
        <span className="text-overflow">{moment(lastModif).fromNow()}</span>
        <Button icon="settings" link={`/config/${gitlabId}/${id}`} />
      </div>
    </li>
  );
};
MyProject.defaultProps = {
  // This value is adopted when name prop is omitted.
};
