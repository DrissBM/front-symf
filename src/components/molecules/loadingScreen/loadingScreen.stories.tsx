import React from "react";

import { LoadingScreen } from "./loadingScreen";
export const Default = () => <LoadingScreen show={true} />;
export const Message = () => <LoadingScreen show={true} message="Chargement" />;
export const Backdrop = () => <LoadingScreen show={true} backdrop />;
export const BackdropMessage = () => (
  <LoadingScreen show={true} backdrop message="Upload en cours" />
);
const component = {
  title: "Molecules/LoadingScreen",
  component: LoadingScreen,
};
export default component;
