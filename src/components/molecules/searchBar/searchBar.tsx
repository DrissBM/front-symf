import React, { FunctionComponent } from "react"; // importing FunctionComponent
import { Button } from "src/components/atoms/button/button";
import { Icon } from "src/components/atoms/icon/icon";
import { bemModifier } from "../../../helpers/bemModifier";

type SearchBarProps = {
  name: string;
  type: string;
  value: string | number | undefined;
  setValue: any;
  label?: string;
};

export const SearchBar: FunctionComponent<SearchBarProps> = ({
  name,
  type,
  value,
  setValue,
  label,
  children,
  ...modifiers
}) => {
  const cleanSearchBar = () => {
    setValue("");
  };
  return (
    <>
      <div className={bemModifier(modifiers, "searchBar")}>
        <label htmlFor={name}>
          <Icon name="search" color="white" />
        </label>
        <input
          value={value}
          onChange={(e) => setValue(e.target.value)}
          placeholder="Search..."
          type={type}
          id={name}
          name={name}
        />
        <Button icon="cross" onClick={cleanSearchBar} />
      </div>
    </>
  );
};
SearchBar.defaultProps = {
  // This value is adopted when name prop is omitted.
};
