import React from "react";
import { SearchBar } from "./searchBar";

export const DefaultSearchBar = () => (
  <SearchBar name="Project name" type="text" value="" setValue={() => console.log("yo")} />
);

const component = {
  title: "Molecules/SearchBar",
  component: SearchBar,
};
export default component;
