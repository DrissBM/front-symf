import React, { FunctionComponent } from "react"; // importing FunctionComponent
import { Project } from "src/components/molecules/project/project";
import { GitlabProjectType } from "src/types/ApiItems";
import { bemModifier } from "../../../helpers/bemModifier";

type ProjectsListProps = {
  /** Color of button */
  projects: Array<GitlabProjectType>;
};

export const ProjectsList: FunctionComponent<ProjectsListProps> = ({
  projects,
  children,
  ...modifiers
}) => {
  const projectsList = projects.map((project, index) => {
    return (
      <Project
        name={project.path}
        lock={project.visibility === "private" ? true : false}
        lastModif={project.last_activity_at}
        id={project.id}
        key={index}
      />
    );
  });
  if (projectsList) {
    return <ul className={bemModifier(modifiers, "projectsList")}>{projectsList}</ul>;
  } else {
    return null;
  }
};
ProjectsList.defaultProps = {
  // This value is adopted when name prop is omitted.
};
