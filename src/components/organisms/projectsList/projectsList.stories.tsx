import React from "react";
import { ProjectsList } from "./projectsList";
import { withKnobs } from "@storybook/addon-knobs";
import { gitlabProjectsMock } from "src/mock/ApiMock";

const projects = gitlabProjectsMock;
export const DefaultProjectsList = () => <ProjectsList projects={projects} />;

const component = {
  title: "Organisms/ProjectsList",
  component: ProjectsList,
  decorators: [withKnobs],
};
export default component;
