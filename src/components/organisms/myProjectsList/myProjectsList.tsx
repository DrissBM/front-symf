import React, { FunctionComponent } from "react"; // importing FunctionComponent
import { MyProject } from "src/components/molecules/myProject/myProject";
import { ProjectType } from "src/types/ApiItems";
import { bemModifier } from "../../../helpers/bemModifier";

type MyProjectsListProps = {
  /** Color of button */
  myProjects: Array<ProjectType>;
};

export const MyProjectsList: FunctionComponent<MyProjectsListProps> = ({
  myProjects,
  children,
  ...modifiers
}) => {
  const projectsList = myProjects.map((project, index) => {
    return (
      <MyProject
        id={project.id}
        gitlabId={project.gitlabId}
        name={project.name}
        techno={project.techno}
        branch={project.branch}
        commit={project.commit}
        lastModif={project.gitlabLastModif}
        URL={project.URL}
        state={project.state}
        key={index}
      />
    );
  });
  if (projectsList) {
    return <ul className={bemModifier(modifiers, "myProjectsList")}>{projectsList}</ul>;
  } else {
    return null;
  }
};
MyProjectsList.defaultProps = {
  // This value is adopted when name prop is omitted.
};
