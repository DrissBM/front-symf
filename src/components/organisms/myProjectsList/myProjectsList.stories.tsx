import React from "react";
import { MyProjectsList } from "./myProjectsList";
import { withKnobs } from "@storybook/addon-knobs";
import { projectsMock } from "src/mock/ApiMock";

const myProjects = projectsMock;
export const DefaultProjectsList = () => <MyProjectsList myProjects={myProjects} />;

const component = {
  title: "Organisms/MyProjectsList",
  component: MyProjectsList,
  decorators: [withKnobs],
};
export default component;
