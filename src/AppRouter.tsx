// eslint-disable-next-line
import React, { useContext, useEffect } from "react";
import { BrowserRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import { AppContext } from "./contexts/appContext";
import { NotFound } from "./page/404/notFound";
import { Authentication } from "./page/authentication/authentication";
import { AuthenticationSuccess } from "./page/authenticationSuccess/authenticationSuccess";
import { Config } from "./page/config/config";

import { Home } from "./page/home/home";
import { Import } from "./page/import/import";
import { Projects } from "./page/projects/projects";

export const AppRouter = () => {
  const { api, apiToken, setApiToken, setUserName } = useContext(AppContext);
  useEffect(() => {
    const ApiTokenStored = localStorage.getItem("apiToken");
    if (ApiTokenStored) {
      if (!apiToken) {
        api.setToken(ApiTokenStored);
        setApiToken(ApiTokenStored);
        api.users
          .getMyUserName()
          .then((res) => {
            setUserName(res);
          })
          .catch(() => {
            localStorage.removeItem("apiToken");
            setApiToken("");
          });
      }
    } else {
      setUserName("");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [apiToken]);
  return (
    <Router>
      {!apiToken && (
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/auth/:token" exact component={AuthenticationSuccess} />
          <Route path="/auth" exact component={Authentication} />
          <Route path="/projects" exact>
            <Redirect to="/auth" />
          </Route>
          <Route path="/import" exact>
            <Redirect to="/auth" />
          </Route>
          <Route>
            <NotFound />
          </Route>
        </Switch>
      )}
      {!!apiToken && (
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/auth" exact>
            <Redirect to="/projects" />
          </Route>
          <Route path="/auth/:token" exact>
            <Redirect to="/projects" />
          </Route>
          <Route path="/projects" exact component={Projects} />
          <Route path="/import" exact component={Import} />
          <Route path="/config/:idGitlab/:idProject?" exact component={Config} />
          <Route path="/config" exact component={Config} />
          <Route component={NotFound} />
        </Switch>
      )}
    </Router>
  );
};
