// eslint-disable-next-line
import React from "react";
import { Box } from "src/components/atoms/box/box";
import { Button } from "src/components/atoms/button/button";
import { Text } from "src/components/atoms/text/text";
import { Page } from "src/layouts/page/Page";

export const Authentication = () => {
  return (
    <Page pageClass="authentication">
      <Box>
        <h1>Authentication</h1>
        <Text type="p1">Choose with wich service do you wanna authenticate</Text>
        {/* <Button color="secondary" iconLeft="gitlab" large tall onClick={getApiToken}>
          Sign In with Gitlab
        </Button> */}
        <Button
          color="secondary"
          iconLeft="gitlab"
          large
          tall
          ahref="http://localhost:8000/api/register"
        >
          Sign In with Gitlab
        </Button>
      </Box>
    </Page>
  );
};
