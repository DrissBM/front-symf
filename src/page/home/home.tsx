// eslint-disable-next-line
import React, { useContext } from "react";
import { Button } from "src/components/atoms/button/button";
import { Text } from "src/components/atoms/text/text";
import { Page } from "src/layouts/page/Page";

// import { AppContext } from "src/contexts/appContext";
export const Home = () => {
  // const { homeItems } = useContext(AppContext);

  return (
    <Page pageClass="home">
      <div className="title">
        <h1>Develop your app</h1>
        <span>then</span>
        <p>spread it</p>
      </div>
      <Text type="p1">
        This app allow you to Deploy your app easily and fast to allow every body to use is it !
      </Text>
      <Button color="black" outline link="/projects">
        Start now
      </Button>
    </Page>
  );
};
