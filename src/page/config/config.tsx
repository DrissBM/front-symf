import React, { useContext, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { Box } from "src/components/atoms/box/box";
import { Button } from "src/components/atoms/button/button";
import { Dropdown } from "src/components/atoms/dropdown/dropdown";
import { Icon } from "src/components/atoms/icon/icon";
import { Input } from "src/components/atoms/input/input";
import { Separator } from "src/components/atoms/separator/separator";
import { Text } from "src/components/atoms/text/text";
import { LoadingScreen } from "src/components/molecules/loadingScreen/loadingScreen";
import { AppContext } from "src/contexts/appContext";
import { Page } from "src/layouts/page/Page";
import {
  environnementVar,
  GitlabProjectType,
  ProjectCreate,
  ProjectType,
} from "src/types/ApiItems";

export const Config = () => {
  const history = useHistory();

  const { api } = useContext(AppContext);

  const { idGitlab } = useParams<any>();
  const [projectGitlab, setProjectGitlab] = useState<GitlabProjectType | null>(null);

  const { idProject } = useParams<any>();
  const [projectApp, setProjectApp] = useState<ProjectType | null>(null);

  const options = ["react", "symfony"];
  const [branch, setBranch] = useState<string>("");
  const [projectName, setProjectName] = useState<string>("");
  const [frameWork, setFrameWork] = useState<"react" | "symfony" | "gitlab">("react");
  const [directory, setDirectory] = useState<string>("./");
  const [buildCommand, setBuildCommand] = useState<string>("npm run build");
  const [outputDirectory, setOutputDirectory] = useState<string>("/build");
  const [installCommand, setInstallCommand] = useState<string>("npm install");

  const [envVariable, setEnvVariable] = useState<Array<environnementVar>>([]);
  const [newEnvName, setNewEnvName] = useState<string>("");
  const [newEnvValue, setNewEnvValue] = useState<string>("");

  const [showModal, setShowModal] = useState<boolean>(false);

  const addEnvVar = () => {
    if (newEnvName && newEnvValue) {
      const newArray: Array<environnementVar> = envVariable;
      newArray.push({ name: newEnvName, value: newEnvValue });
      setEnvVariable(newArray);
      setNewEnvName("");
      setNewEnvValue("");
    }
  };

  const rmEnvVar = (index: number) => {
    let newArray: Array<environnementVar> = envVariable.filter(
      (item) => item.name !== envVariable[index].name || item.value !== envVariable[index].value
    );
    setEnvVariable(newArray);
  };

  const getCreateData = (): ProjectCreate | null => {
    if (projectGitlab) {
      return {
        name: projectName,
        techno: frameWork,
        rootDir: directory,
        buildCmd: buildCommand,
        outputDir: outputDirectory,
        installCmd: installCommand,
        envVar: envVariable,
        gitlabId: projectGitlab.id,
        gitlabName: projectGitlab.path,
        locked: projectGitlab.visibility === "private" ? true : false,
        branch: branch,
        commit: projectGitlab.branch.find((a) => a.name === branch).commit,
        gitlabLastModif: projectGitlab.last_activity_at,
        gitlabPath: projectGitlab.path_with_namespace,
      };
    } else {
      return null;
    }
  };

  const getUpdateData = (): any => {
    return {
      id: idProject,
      ...getCreateData(),
    };
  };

  const create = async () => {
    setShowModal(true);
    const data = getCreateData();
    if (data) {
      const success: any = await api.projects.create(data);
      if (success) {
        api.projects.deployProject(success.id);
        setTimeout(() => {
          history.push("/projects");
        }, 500);
      } else {
        alert("ERROR");
      }
    }
  };
  const update = async () => {
    setShowModal(true);
    const success: any = await api.projects.update(getUpdateData());
    if (success) {
      await api.projects.removeProject(idProject);
      api.projects.deployProject(idProject);
      setTimeout(() => {
        history.push("/projects");
      }, 500);
    } else {
      alert("ERROR");
    }
  };

  const deleteProject = async () => {
    // eslint-disable-next-line no-restricted-globals
    const conf = confirm("Do you really want to delete this project ?");
    if (conf) {
      setShowModal(true);
      const success: any = await api.projects.removeProject(idProject);
      if (success) {
        const done: any = await api.projects.removeProjectRepo(idProject);
        if (done) {
          history.push("/projects");
        } else {
          alert("ERROR");
        }
      } else {
        alert("ERROR");
      }
    }
  };

  useEffect(() => {
    api.gitlab.getProject(idGitlab).then((result) => {
      const branches: Array<any> = [];
      const branchesDetailed: Array<any> = result[1];
      branchesDetailed.forEach((element) =>
        branches.push({ name: element.name, commit: element.commit.title })
      );
      setProjectGitlab({
        id: result[0].id,
        path: result[0].path,
        branch: branches,
        default_branch: result[0].default_branch,
        last_activity_at: result[0].last_activity_at,
        path_with_namespace: result[0].path_with_namespace,
        visibility: result[0].visibility,
      });
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [idGitlab]);

  useEffect(() => {
    if (projectGitlab) {
      if (idProject) {
        api.projects.getProject(idProject).then((result) => {
          setProjectApp(result);
        });
      } else {
        setProjectName(projectGitlab.path);
        setBranch(projectGitlab.default_branch);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [projectGitlab, idProject]);

  useEffect(() => {
    if (projectApp) {
      setBranch(projectApp.branch);
      setProjectName(projectApp.name);
      setFrameWork(projectApp.techno);
      setDirectory(projectApp.rootDir);
      setBuildCommand(projectApp.buildCmd);
      setOutputDirectory(projectApp.outputDir);
      setInstallCommand(projectApp.installCmd);
      setEnvVariable(projectApp.envVar);
    }
  }, [projectApp]);

  return (
    <Page pageClass="config">
      {idProject ? (
        <div className="flex-inline title">
          <h1>Settings</h1>
          <Button color="red" onClick={deleteProject}>
            Delete
          </Button>
        </div>
      ) : (
        <h1>Last Step !</h1>
      )}
      <div className="config-gitlab">
        <Box className="configuration">
          <div className="flex-inline">
            <Text type="p2">Configuration</Text>
            {projectApp && <div className={`pastille ${projectApp.state}`}></div>}
          </div>
          <Separator>General</Separator>
          <Input name="Project name" type="text" value={projectName} setValue={setProjectName} />
          <Dropdown
            name="Framework preset"
            options={options}
            value={frameWork}
            setValue={setFrameWork}
            icon
          />
          <Input
            name="Root directory"
            type="text"
            value={directory}
            setValue={setDirectory}
            disabled
            editable
          />

          <Separator>Build and Output Setting</Separator>
          <Input
            name="Build command"
            type="text"
            value={buildCommand}
            setValue={setBuildCommand}
            disabled
            editable
          />
          <Input
            name="Output directory"
            type="text"
            value={outputDirectory}
            setValue={setOutputDirectory}
            disabled
            editable
          />
          <Input
            name="Install Command"
            type="text"
            value={installCommand}
            setValue={setInstallCommand}
            disabled
            editable
          />

          <Separator>Environnement Variables</Separator>
          <div className="newEnvVariable title">
            <p>Name</p>
            <p>Value</p>
            <Button color="white" outline>
              Add
            </Button>
          </div>
          <div className="newEnvVariable">
            <Input
              name="Name"
              type="text"
              value={newEnvName}
              placeholder="ENV_NAME"
              setValue={setNewEnvName}
              noTitle
            />
            <Input
              name="Value"
              type="text"
              value={newEnvValue}
              setValue={setNewEnvValue}
              placeholder="A54SDW85JGH1FD2"
              noTitle
            />
            <Button color="white" outline onClick={addEnvVar}>
              Add
            </Button>
          </div>
          {envVariable.length > 0 && (
            <>
              <div className="envVariable title">
                <p>Name</p>
                <p>Value</p>
                <Button icon="cross" />
              </div>
              {envVariable.map((envVar, index) => (
                <div className="envVariable value" key={index}>
                  <p>{envVar.name}</p>
                  <p>{envVar.value}</p>
                  <Button icon="cross" onClick={() => rmEnvVar(index)} />
                </div>
              ))}
            </>
          )}
        </Box>
        <div className="gitlab">
          <Box>
            <div className="space-between">
              <Text type="p2">Gitlab</Text>
              <Icon name="gitlab" />
            </div>
            <Input
              name="Project"
              type="text"
              value={projectGitlab ? projectGitlab.path : ""}
              setValue={() => console.warn("You can't change this value")}
              disabled
            />
            <Dropdown
              name="Branch"
              options={projectGitlab ? projectGitlab.branch.map((a) => a.name) : ""}
              value={branch}
              setValue={setBranch}
            />
          </Box>
          <div>
            {projectApp ? (
              <Button color="primary" large onClick={update}>
                ReDeploy
              </Button>
            ) : (
              <Button color="primary" large onClick={create}>
                Deploy
              </Button>
            )}
          </div>
        </div>
      </div>
      <LoadingScreen show={showModal} message="Requète en cours de traitement" backdrop />
    </Page>
  );
};
