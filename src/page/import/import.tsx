// eslint-disable-next-line
import React, { useContext, useEffect, useState } from "react";
import { Box } from "src/components/atoms/box/box";
import { Icon } from "src/components/atoms/icon/icon";
import { ProjectsListSkeleton } from "src/components/atoms/skeleton/projectsListSkeleton";
import { Text } from "src/components/atoms/text/text";
import { SearchBar } from "src/components/molecules/searchBar/searchBar";
import { ProjectsList } from "src/components/organisms/projectsList/projectsList";
import { AppContext } from "src/contexts/appContext";
import { Page } from "src/layouts/page/Page";
import { GitlabProjectType } from "src/types/ApiItems";

export const Import = () => {
  const { api } = useContext(AppContext);

  const showXresults: number = 5;
  const [filter, setFilter] = useState<string>("");
  const [projectsFiltred, setProjectsFiltred] = useState<Array<GitlabProjectType> | null>(null);
  const [projects, setProjects] = useState<Array<GitlabProjectType> | null>(null);

  const filterProject = (newFilter: string) => {
    if (projects) {
      return projects.filter((el) => {
        return el.path.toLowerCase().indexOf(newFilter.toLowerCase()) !== -1;
      });
    } else {
      return null;
    }
  };

  useEffect(() => {
    api.gitlab.getAll().then((result) => {
      setProjects(result);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setProjectsFiltred(projects);
  }, [projects]);

  useEffect(() => {
    let newArray = filterProject(filter);
    setProjectsFiltred(newArray);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);

  return (
    <Page pageClass="import">
      <h1>Import projects</h1>
      <div className="project-advice">
        <Box className="list">
          <div className="space-between">
            <Text type="p2">Choose your project</Text>
            <Icon name="gitlab" />
          </div>
          <SearchBar name="import" type="text" value={filter} setValue={setFilter} />
          {projectsFiltred ? (
            projectsFiltred.length > 0 ? (
              <ProjectsList projects={projectsFiltred.slice(0, showXresults)} />
            ) : (
              "No project found"
            )
          ) : (
            <ProjectsListSkeleton numberItem={5} />
          )}
        </Box>
        <Box className="advice">
          <Text type="p1">There you have to choose wich project do you wanna deploy</Text>
        </Box>
      </div>
    </Page>
  );
};
