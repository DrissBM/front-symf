// eslint-disable-next-line
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { Box } from "src/components/atoms/box/box";
import { Page } from "src/layouts/page/Page";

export const AuthenticationSuccess = () => {
  const { token } = useParams<any>();

  useEffect(() => {
    if (token) {
      localStorage.setItem("apiToken", token);
    }
  }, [token]);

  return (
    <Page pageClass="authenticationSuccess">
      <Box>
        <h1>Authentication Success !</h1>
      </Box>
    </Page>
  );
};
