// eslint-disable-next-line
import React from "react";
import { Page } from "src/layouts/page/Page";
import { Text } from "src/components/atoms/text/text";

export const NotFound = () => {
  return (
    <Page pageClass="notFound">
      <h1>Error 404</h1>
      <Text type="p1">Page Not Found. Make sure the address is correct.</Text>
    </Page>
  );
};
