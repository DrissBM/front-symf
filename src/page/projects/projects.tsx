// eslint-disable-next-line
import React, { useContext, useEffect, useState } from "react";
import { Button } from "src/components/atoms/button/button";
import { SearchBar } from "src/components/molecules/searchBar/searchBar";
import { MyProjectsList } from "src/components/organisms/myProjectsList/myProjectsList";
import { Page } from "src/layouts/page/Page";
import { ProjectType } from "src/types/ApiItems";
import { useMediaQuery } from "react-responsive";
import { AppContext } from "src/contexts/appContext";
import { MyProjectsListSkeleton } from "src/components/atoms/skeleton/myProjectsListSkeleton";
// import { AppContext } from "src/contexts/appContext";

export const Projects = () => {
  const { api } = useContext(AppContext);

  const [filter, setFilter] = useState<string>("");
  const [projectsFiltred, setProjectsFiltred] = useState<Array<ProjectType> | null>(null);
  const [myProjects, setMyProjects] = useState<Array<ProjectType> | null>(null);

  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 768px)",
  });

  const filterProject = (newFilter: string) => {
    if (myProjects) {
      return myProjects.filter((el) => {
        return el.name.toLowerCase().indexOf(newFilter.toLowerCase()) !== -1;
      });
    } else {
      return null;
    }
  };

  useEffect(() => {
    api.projects.getAllMy().then((result) => {
      setMyProjects(result);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setProjectsFiltred(myProjects);
  }, [myProjects]);

  useEffect(() => {
    const isOnHold = myProjects?.find((project) => project.state === "onHold");
    if (isOnHold) {
      setTimeout(() => {
        api.projects.getAllMy().then((result) => {
          setMyProjects(result);
        });
      }, 2000);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [myProjects]);

  useEffect(() => {
    let newArray = filterProject(filter);
    setProjectsFiltred(newArray);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter]);
  return (
    <Page pageClass="projects">
      <h1>My projects</h1>
      <div className="searchProject">
        <SearchBar name="project" type="text" value={filter} setValue={setFilter} />
        {isDesktopOrLaptop ? (
          <Button color="white" icon="add" outline tall link="/import">
            Add project
          </Button>
        ) : (
          <Button color="white" icon="add" outline tall link="/import" />
        )}
      </div>
      {projectsFiltred ? (
        projectsFiltred.length > 0 ? (
          <MyProjectsList myProjects={projectsFiltred} />
        ) : (
          "No project found"
        )
      ) : (
        <MyProjectsListSkeleton numberItem={5} />
      )}
    </Page>
  );
};
