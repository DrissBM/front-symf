import { GitlabProjectType } from "src/types/ApiItems";
import { FetchApi } from "./FetchApi";

export class GitlabApi extends FetchApi {
  getAll() {
    return this.get<GitlabProjectType[]>(`/gitlab_projects`);
  }
  getProject(id: number) {
    return this.get<GitlabProjectType>(`/gitlab_projects/${id}`);
  }
}
