import axios, { AxiosInstance, AxiosError, AxiosRequestConfig } from "axios";
import { version } from "../../package.json";
import merge from "lodash/merge";

export class FetchApi {
  baseUrl: string | undefined;
  axios: AxiosInstance;
  token: string;
  version: string;
  errorCallback: (e: AxiosError) => void;
  axiosOptions: any;
  constructor(errorCallback, axiosOptions) {
    this.baseUrl = process.env.REACT_APP_API_BASE
      ? process.env.REACT_APP_API_BASE
      : "http://localhost:8000";
    this.token = process.env.REACT_APP_API_TOKEN ? process.env.REACT_APP_API_TOKEN : "";
    this.axios = axios.create();
    this.version = version;
    this.axiosOptions = axiosOptions;
    this.errorCallback = errorCallback;
  }
  private merge(obj1, obj2) {
    return merge(Object.assign({}, obj1), Object.assign({}, obj2));
  }

  protected get<T = any>(endpoint: string): Promise<T> {
    return new Promise(async (resolve, reject) => {
      const url = this.baseUrl + endpoint;
      this.axios
        .get(url, this.axiosOptions)
        .then((response) => {
          resolve(response.data);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }
  protected getAny<T = any>(url: string): Promise<T> {
    return this.get(url);
  }

  protected post<T = any>(
    endpoint: string,
    data: any,
    addionnalAxiosOptions?: AxiosRequestConfig
  ): Promise<T> {
    return new Promise((resolve, reject) => {
      const url = this.baseUrl + endpoint;
      this.axios
        .post(url, data, this.merge(this.axiosOptions, addionnalAxiosOptions))
        .then((response) => {
          const data = response.data ? response.data : response;
          resolve(data);
        })
        .catch((e) => {
          // this.errorCallback(e);
          reject(e);
        });
    });
  }

  protected put<T = any>(
    endpoint: string,
    data: any,
    addionnalAxiosOptions?: AxiosRequestConfig
  ): Promise<T> {
    return new Promise((resolve, reject) => {
      const url = this.baseUrl + endpoint;
      this.axios
        .put(url, data, this.merge(this.axiosOptions, addionnalAxiosOptions))
        .then((response) => {
          const data = response.data ? response.data : response;
          resolve(data);
        })
        .catch((e) => {
          // this.errorCallback(e);
          reject(e);
        });
    });
  }
  protected delete<T = any>(endpoint: string): Promise<T> {
    return new Promise((resolve, reject) => {
      const url = this.baseUrl + endpoint;
      this.axios
        .delete(url, this.axiosOptions)
        .then((response) => {
          const data = response.data ? response.data : response;
          resolve(data);
        })
        .catch((e) => {
          // this.errorCallback(e);
          reject(e);
        });
    });
  }
  protected putFormData<T = any>(endpoint: string, data: FormData): Promise<T> {
    return this.put(endpoint, data, {
      headers: { "Content-Type": "multipart/form-data" },
    });
  }
  protected postFormData<T = any>(endpoint: string, data: FormData): Promise<T> {
    return this.post(endpoint, data, {
      headers: { "Content-Type": "multipart/form-data" },
    });
  }

  protected objectToFormdata(item, fileskeys: string[] = []) {
    const formdata = new FormData();
    for (const key in item) {
      if (fileskeys.includes(key)) {
        item[key].forEach((element) => {
          formdata.append(key, element);
        });
      } else {
        formdata.append(key, item[key]);
      }
    }
    return formdata;
  }
  protected objectToQuery(params?: {}) {
    if (!params) return "";
    let paramsArray: string[] = [];
    Object.keys(params).forEach((key) => {
      if (params[key]) {
        paramsArray.push(key + "=" + params[key]);
      }
    });
    return paramsArray.length ? "?" + paramsArray.join("&") : "";
  }
}
