import axios, { AxiosInstance, AxiosError } from "axios";
import { ProjectsApi } from "./ProjectsApi";
import { UsersApi } from "./UsersApi";
import { GitlabApi } from "./GitlabApi";

export class SpreadApi {
  axios: AxiosInstance;
  axiosOptions: any;
  projects: ProjectsApi;
  users: UsersApi;
  gitlab: GitlabApi;
  private errorHanlders: { (data?: AxiosError): void }[] = [];
  constructor() {
    this.axios = axios.create();
    this.axiosOptions = {};
    this.projects = new ProjectsApi((data) => this.errorCallback(data), this.axiosOptions);
    this.users = new UsersApi((data) => this.errorCallback(data), this.axiosOptions);
    this.gitlab = new GitlabApi((data) => this.errorCallback(data), this.axiosOptions);
    this.errorHanlders = [];
  }
  setToken(token) {
    this.axiosOptions["headers"] = { Authorization: "Bearer " + token };
  }
  // async checkToken(token) {
  //   const { REACT_APP_OAUTH_INTROSPECT_URL, REACT_APP_OAUTH_CLIENT_ID } = process.env;
  //   const message = `client_id=${REACT_APP_OAUTH_CLIENT_ID}&token=${token}`;
  //   const response = await this.axios.post(REACT_APP_OAUTH_INTROSPECT_URL!, message, {
  //     headers: { "Content-Type": "text/plain" },
  //   });
  //   if (response.data && response.data.active) {
  //     return true;
  //   }
  //   return;
  // }
  onError(handler) {
    this.errorHanlders.push(handler);
  }
  errorCallback(e: AxiosError) {
    this.errorHanlders.slice(0).forEach((handler) => handler(e));
  }
}
