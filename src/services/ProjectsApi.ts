// import { Announce, AnnounceDetail } from "src/types/ApiItems";
import { ProjectType, ProjectCreate } from "src/types/ApiItems";
import { FetchApi } from "./FetchApi";

export type ProjectUpdate = ProjectCreate & {
  id: number;
};

export class ProjectsApi extends FetchApi {
  getAll() {
    return this.get<ProjectType[]>(`/projects`);
  }
  getAllMy() {
    return this.get<ProjectType[]>(`/myproject`);
  }
  getProject(id: number) {
    return this.get<ProjectType>(`/projects/${id}`);
  }
  deployProject(id: number) {
    return this.get<any>(`/deploy/${id}`);
  }
  removeProject(id: number) {
    return this.get<any>(`/remove/${id}`);
  }
  removeProjectRepo(id: number) {
    return this.get<any>(`/removeRepo/${id}`);
  }
  async create(project: ProjectCreate): Promise<false | number> {
    try {
      return await this.post<any>(`/projects`, project);
    } catch (e) {
      return false;
    }
  }
  update(project: ProjectUpdate) {
    return this.put<any>(`/projects/${project.id}`, project);
  }
  remove(id: number) {
    return this.delete<any>(`/projects/${id}`);
  }
  // private deleteAnnounceFromStorage(id) {
  //   this.removeFromStorage(`/smallAnnounces/${id}`);
  // }
}
